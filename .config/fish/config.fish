set -x FZF_DEFAULT_COMMAND "fd --type f --strip-cwd-prefix --hidden --follow --exclude .git"
set -x FZF_CTRL_T_COMMAND $FZF_DEFAULT_COMMAND
set -x FZF_ALT_C_COMMAND "fd --type d --strip-cwd-prefix --hidden --follow --exclude .git"
set -x HOMEBREW_NO_GITHUB_API 1
set -x JAVA_HOME /Library/Java/JavaVirtualMachines/jdk-21-oracle-java-se.jdk/Contents/Home
set -x RIPGREP_CONFIG_PATH $HOME/.ripgreprc
set -x VISUAL nvim
fish_add_path $HOME/.cargo/bin
fish_add_path $HOME/go/bin
fish_add_path /opt/local/bin
fish_add_path /opt/local/sbin
alias cat=bat
alias groot="cd (git root)"
alias ls=lsd
alias vi=nvim
alias vim=nvim
status --is-interactive; and rbenv init - fish | source
status --is-interactive; and source (nodenv init -|psub)
pyenv init - | source
zoxide init fish | source
starship init fish | source
