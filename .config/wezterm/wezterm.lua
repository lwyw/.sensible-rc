local wezterm = require("wezterm")

local keys = {}
-- printable characters
for i = 32,127 do
  k = string.char(i)
  table.insert(keys, {
    key = k,
    mods = "ALT",
    action = wezterm.action.SendString("\x1b" .. k),
  })
end

return {
  color_scheme = "tokyonight",
  enable_tab_bar = false,
  keys = keys,
}
