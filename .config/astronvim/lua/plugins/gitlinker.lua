return {
  "linrongbin16/gitlinker.nvim",
  event = "User AstroGitFile",
  config = function()
    require("gitlinker").setup()
  end,
}
