return {
  "Wansmer/treesj",
  event = "User AstroFile",
  dependencies = "nvim-treesitter/nvim-treesitter",
  config = function()
    require("treesj").setup()
  end,
}
