return {
  {
    "AstroNvim/astrocore",
    ---@type AstroCoreOpts
    opts = {
      options = {
        opt = {
          showcmdloc = "statusline",
        },
        g = {
          loaded_perl_provider = 0,
        },
      },
    },
  },
}
